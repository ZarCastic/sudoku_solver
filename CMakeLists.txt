project(sudoku_solverSolver)
cmake_minimum_required(VERSION 3.9)

find_package(Qt5 COMPONENTS REQUIRED Core Gui Widgets)

include(tests/CMakeLists.txt)

set(CMAEK_BUILD_TYPE Debug)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

include_directories("inc")
set(SOURCES ${SOURCES} 
)

QT5_WRAP_CPP(SOURCES
)

add_executable(sudoku_solver src/main.cpp ${SOURCES})
target_link_libraries(sudoku_solver Qt5::Widgets Qt5::Core)

add_executable(sudoku_solver_tests tests/main_tests.cpp ${TEST_SOURCES} ${SOURCES})
target_link_libraries(sudoku_solver_tests ${GTEST_BOTH_LIBRARIES} Qt5::Widgets Qt5::Core)
add_test(AllTestsInsudoku_solver sudoku_solver_tests)
